# SvelteKit et SQLite3

Notes d'après le tutoriel de [P. Hartenfeller](https://www.youtube.com/watch?v=iO4VUbQ6ua4)

Comment faire interagir SQLite3 (ou n'importe quelle autre base de données) avec SK (version 1+ de SvelteKit) ?

Nous allons voir :

- comment intégrer une base de données ici SQLite
- comment dialoguer avec la base de données
- comment utiliser les actions nommées avec SK (et leur intérêt) ?
- comment faire un endpoint pour une API REST avec SK ?
  
Et nous utiliserons TypeScript et pour les styles nous utiliserons la bibliothèque Bulma.

## 1-Pré-requis et installation

Pré-requis :

- télécharger la base de données sur [sqlitetutorial.net](https://www.sqlitetutorial.net/sqlite-sample-database/)
- savoir comment créer un projet SK et avoir les éléments de base de Svelte

Installation :

- installer les éléments SvelteKit de façon traditionnelle
- créer un répertoire `data` au niveau de la racine du projet et y placer la base de données dézippée
- installer la bibliothèque [better-sqlite3](https://www.npmjs.com/package/better-sqlite3) : `npm i better-sqlite3`
- installer les types puisque nous travaillons avec TS : `npm i --save-dev @types/better-sqlite3`
- créer dans le répertoire `src`, un nouveau répertoire `lib/db`

## 2-Gestion de la connexion avec SQLite3

Créons un fichier dans `./src/lib/db` nommé `database.ts` et à la racine du projet créons un fichier `.env` dans lequel on va rajouter le chemin vers la base de données.

```txt
DB_PATH="./data/chinook.db"
```

Maintenant dans `database.ts`, nous allons importer la bibliothèque better-sqlite3 et notre variable d'environnement :

```ts
import Database from "better-sqlite3";
import { DB_PATH } from '$env/static/private';
```

Puis on va se connecter à l'aide de l'objet `Database` :

```ts
const db = Database(DB_PATH, {verbose: console.log});
```

Nous avons récupéré la variable d'environnement pour utiliser le chemin d'accès vers la base de données et nous avons passé une option pour afficher les logs de façon verbeuse à la console.

Puis nous allons créer une première fonction qui va retourner les 50 premières données selon les critères suivants :

- l'ID de la piste
- le nom de la piste
- l'ID de l'album
- le titre de l'album
- l'ID de l'artiste
- le nom de l'artiste
- le genre de musique

Nous allons limiter à 50 éléments.

Avec la bibliothèque better-sqlite3, nous allons d'abord appeler la méthode prepare() qui va récupérer notre requête SQL suivante :

```sql
select 
    t.TrackId as trackId, 
    t.Name as trackName, 
    a.AlbumId as albumId, 
    a.Title as albumTitle, 
    at.ArtistId as artistId, 
    at.Name as artistName, 
    g.Name as genre
from tracks t 
    join albums a 
        on t.AlbumId = a.AlbumId 
    join artists at 
        on a.ArtistId = at.ArtistId 
    join genres 
        g on t.GenreId = g.GenreId
limit 50
```

Puis nous allons retourner un tableau de données avec la méthode `All()` :

```ts
export const getInitialTracks = (limit: number): any[] => {

    const sql = `select 
    t.TrackId as trackId, 
    t.Name as trackName, 
    a.AlbumId as albumId, 
    a.Title as albumTitle, 
    at.ArtistId as artistId, 
    at.Name as artistName, 
    g.Name as genre
from tracks t 
    join albums a 
        on t.AlbumId = a.AlbumId 
    join artists at 
        on a.ArtistId = at.ArtistId 
    join genres 
        g on t.GenreId = g.GenreId
limit ${limit}`;
    
    return db.prepare(sql).all();
};
```

Pour stocker nos données nous allons nous créer un type Track tout simple. Dans `lib`, créons un fichier `types.ts` (et non `.d.ts` !). Pourquoi ? par défaut, TS nous indique que notre fonction retourne un type `any[]` mais cela n'est pas pertinent et trop laxiste. Nous pourrions indiquer `unknown` comme proposé par le compilateur. Mais nous allons utiliser notre type `Track` pour forcer le typage :

Donc le type sera structuré très simplement. Les noms de chaque propriété sera l'alias du code SQL :

```ts
export type Track = {
    trackId: number;
    trackName: string;
    albumId: number;
    albumTitle: string;
    artistId: number;
    artistName: string;
    genre: string;
};
```

Le nom, par exemple, `trackId` est **le même entre le code SQL et notre type `Track`**. Cela va faciliter la sérialisation des données récupérées depuis SQLite  vers notre type.

Maintenant nous pouvons récupérer notre nouveau type dans `database.ts` :

```ts
import type { Track } from "./types";

...

export const getInitialTracks = (limit: NumericLiteral): Track[] => {
    ...
    return db.prepare(sql).all() as Track[];
};
```

### Gestion de l'argument

Nous pouvons passer l'argument à `all()` comme suit : `db.prepare(sql).all({limit})` mais dans notre variable `sql`, il faut apporter la modification de `limit ${limit}` à `limit $limit`.

Le résultat est équivalent mais cela alourdit inutilement le code, sa maintenance est affaiblie.

## 3-Chargement des données

### 1-Avec +page.server.ts

Nous souhaitons afficher dans notre page racine `./src/routes/+page.svelte` les données. Alors nous devons créer un fichier particulier dans le même répertoire `./src/routes/+page.server.ts`. Ce fichier va agir comme si c'était une action côté serveur : on va faire une requête vers la base de données et retourner les données. Dans ce fichier nous allons charger les données donc la fonction à exporter est `load()` et si on lit la documentation de SK, le squelette de notre fonction ressemblera à :

```ts
import type { PageServerLoad } from './$types';

export const load = (async () => {
    return {};
}) satisfies PageServerLoad;
```

Et très simplement, nous allons :

- importer notre fonction `getInitialTracks` de la bibliothèque database.ts
- utiliser cette fonction
- retourner les résultats dans un objet avec un champ, par exemple, `tracks`

```ts
import { getInitialTracks } from '$lib/database';
import type { PageServerLoad } from './$types';

export const load = (async () => {
    const rlst = getInitialTracks(10);
    return {
        tracks: rlst
    };
}) satisfies PageServerLoad;
```

Et c'est tout.

### 2-Affichage des données

Dans +page.svelte, nous devons récupérer les données et pour cela nous devons utiliser le mot-clé `data`. Mais comme nous utilisons TS, il faut préciser le type pour `data`. Ce type est `PageData`. N'oublions pas l'importation de ce type :

```html
<script lang="ts">
    import type { PageData } from './$types';
    export let data: PageData;
</script>

<div>
    <pre>{JSON.stringify(data.tracks, null, 2)}</pre>
</div>
```

## 4-Stylisation des données

L'affichage est brut. Nous allons transformer les données pour qu'elles soient affichées sous forme de tableau.

Nous n'allons pas utiliser TailwindCSS mais un autre framework : [Bulma](https://bulma.io/) en version CDN. Donc nous allons rajouter les éléments suivants dans `./src/app.html` :

```html
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css">
```

Pour récupérer les données et les afficher dans un tableau, nous allons utiliser la directive `{#each}`. Voici le code complet de génération de notre tableau :

```html
<div class="px-4">

<h1 class="is-size-1">Tracks</h1>
<table class="table">
<thead>
    <tr>
        <th>Track</th>
        <th>Artist</th>
        <th>Album</th>
        <th>Genre</th>
    </tr>
</thead>
<tbody>
    {#each data.tracks as t}
        <tr>
            <td>{t.trackName}</td>
            <td>{t.artistName}</td>
            <td>{t.albumTitle}</td>
            <td>{t.genre}</td>
        </tr>
    {/each}
</tbody>
</table>
</div>
```

## 5-Création d'une route pour les albums

### 1-Préparation de l'arborescence

Nous souhaitons afficher le contenu d'un album spécifique dans une page, une route spécifique.

Pour l'instant nous allons tout simplement créé un lien `<a>` sur le titre de l'album :

```html
...
<tbody>
    {#each data.tracks as t}
        <tr>
            <td>{t.trackName}</td>
            <td>{t.artistName}</td>
            <td><a href={`/album/${t.albumId}`}>{t.albumTitle}</a></td>
            <td>{t.genre}</td>
        </tr>
    {/each}
</tbody>
```

Donc l'URL sera généré à notre place : `/album/1`, etc.

Créons une route : `./src/routes/album/[id]` donc nous aurons 2 répertoires à créer `album` et `[id]`. Ce dernier est sous-répertoire d'`album` et la présence des crochets indique que nous allons pouvoir récupérer la valeur en tant que paramètre de requête.

A l'intérieur de `[id]`, rajoutons un fichier `+page.svelte` et `+page.ts`.

### 2-Récupération de l'ID

Tout d'abord récupérons l'id. Cela n'est possible que dans `+page.server.ts`. Il y a une variable fournie par SK : `params`

Si on reprend la documentation de SK, alors le squelette de notre application ressemblera à :

```ts
import type { PageLoad } from './$types';

export const load = (async () => {
    
    return { };
}) satisfies PageLoad;
```

Maintenant récupérons l'ID avec l'aide de la variable `params` :

```ts
import type { PageLoad } from './$types';

export const load = (async ({ params }) => {
    
    return {
        id: params.id
    };
}) satisfies PageLoad;
```

Du côte de `+page.svelte`, nous allons tout simplement récupérer la donnée :

```html
<script lang="ts">
    import type { PageData } from './$types';
    
    export let data: PageData;
</script>

<h1>Album</h1>
<div>{data.id}</div>
```

### 3-Interrogation de la base de données

Maintenant que savons récupérer l'ID. Passons à la requête sur SQLite. Dans `./src/lib/database.ts`

Créons une nouvelle fonction prenant en paramètre un number (l'id) et retournera un type que nous définirons immédiatement après.

D'abord créons une simple requête SQL. Puis au lieu d'utiliser all() nous utiliserons get() qui nous retournera le premier enregistrement satisfaisant notre requête SQL.

Nous allons non pas passer directement notre argument dans la chaîne mais via `get()` et il faudra bien faire attention à l'écriture de la requête car il n'y a pas d'interpolation de chaîne :

```ts
export const getAlbumById = (id: number) => {
    const sql = `select 
   a.AlbumId as albumId,
   a.Title as albumTitle,
   at.ArtistId as artistId,
   at.Name as artistName
from albums a
join artists at on a.AlbumId = at.ArtistId
where a.AlbumId = $id`;
    
    const rslt = db.prepare(sql).get({id})
    return rslt as Album;
};
```

Quant à notre type de retour, lui est très simple. Dans types.ts, rajoutons le code suivant :

```ts
export type Album = {
    albumId: number;
    albumTitle: string;
    artistId: number;
    artistName: string;
};
```

Créons une autre fonction qui va lister les pistes de l'album sur le même modèle que ci-dessus :

- dans `database.ts` :

```ts
export const getAlbumTracks = (id: number) => {
    const sql = `select 
 t.TrackId as trackId,
 t.Name as trackName,
 t.Milliseconds as trackMs
from tracks t
where t.AlbumId = $id
order by t.TrackId`;
    const rslt = db.prepare(sql).all({ id });
    return rslt as AlbumTracks[];
}
```

- dans `types.ts` :

```ts
export type AlbumTracks = {
    trackId: number;
    trackName: string;
    trackMs: number;
};
```

### 4-Gestion de nos requêtes

Retour dans `./src/routes/album/[id]/+page.server.ts` et là nous allons passer notre id récupérer pour le passer à nos deux requêtes. Les résultats seront stockés dans un objet quelconque :

```ts
import { getAlbumById, getAlbumTracks } from '$lib/database';
import type { PageServerLoad } from './$types';

export const load = (({ params }) => {
    const id = +params.id;

    const album = getAlbumById(id)
    const tracks = getAlbumTracks(id);

    return {
        id,
        album,
        tracks
    };
}) satisfies PageServerLoad;
```

Rien de compliqué ! Nous appelons nos deux fonctions exportées, nous récupérons les données et les retournons dans un objet quelconque.

Maintenant nous allons pouvoir récupérer les données et les placer dans le `+page.svelte`

### 5-Mise en place graphique

Le plus important ici sera la récupération des données. Pour éviter des problèmes avec l'autocomplétion les types, nous allons forcer une conversion. Les données reçues sont de type `PageData` et nous voulons un objet avec un id, un type Album et un tableau d'AlbumTracks :

```html
<script lang="ts">
    import type { Album, AlbumTracks } from '$lib/types';
    import type { PageData } from './$types';
    
    export let data: PageData;
    // conversion
    const d = data as {
        id: number;
        album: Album;
        tracks: AlbumTracks[]
    }
    
</script>

<div class="px-4">

<h1 class="is-size-1">Album : {d.album.albumTitle}</h1>

<p class="is-size-4">{d.album.artistName}</p>
</div>

<div>
<table class="table mt-6">
<thead>
    <tr>
        <th>#</th>
        <th>Piste</th>
        <th>Temps</th>
    </tr>
</thead>
<tbody>
    {#each d.tracks as t, i}
        <tr>
            <td>{i +1}</td>
            <td>{t.trackName}</td>
            <td>{Math.floor(t.trackMs/1000)}s</td>
        </tr>
    {/each}
</tbody>
</table>
</div>
```

## 6-Gestion des erreurs

Documentation officielle : [https://kit.svelte.dev/docs/errors](https://kit.svelte.dev/docs/errors)

### 1-L'erreur de parsing

Si on se trompe de route nous avons une erreur 500. Par exemple, nous utilisons une route `/album/sdds`, il y a appel de la fonction `load()` mais les données transmises à SQLite sont de type "null" via l'appel `getAlbumById()`.

Nous pouvons dans un premier temps vérifier la valeur. Que retourne `id` si on passe une série de lettres ? un `NaN`. Donc avec TS nous allons utiliser la méthode `isNaN()` associée au type `Number` qui retournera un booléen.

Puis si la valeur analysée est un `NaN` alors on retourne une erreur mais selon la méthode de SK. Pour cela, SK propose la fonction `error()` importée depuis `@sveltejs/kit` :

```ts
import { error } from '@sveltejs/kit';
```

Cette fonction prend deux paramètres :

- un code status HTTP
- un message quelconque

Ce qui nous donne :

```ts
export const load = (({ params }) => {
    const id = +params.id;

    if (Number.isNaN(id)) {
        throw error(404, "ID est non valide")
    }
    
    // le reste du code n'est pas modifié

}) satisfies PageServerLoad;
```

### 2-Erreur ID inexistant

Si maintenant nous passons bien un nombre mais que cet ID n'existe pas : 10000000 ? nous obtenons encore une erreur 500.

Côté log serveur, nous avons une erreur `Cannot read properties of undefined (reading 'albumTitle')`. Donc à nouveau notre `+page.server.ts` va servir d'interface pour gérer l'absence de données.

En fait le premier élément erroné lu est stocké dans la constante album. Donc si elle est undefined alors on retourne une erreur :

```ts
export const load = (({ params }) => {
    const id = +params.id;

    if (Number.isNaN(id)) {
        throw error(404, "ID est non valide")
    }
    const album = getAlbumById(id)
    const tracks = getAlbumTracks(id);

    if (!album) {
        throw error(404, `Pas d'album avec l'ID ${id}`)
    }

    // on ne modifie pas le retour

}) satisfies PageServerLoad;
```

### Customisation de l'affichage

Par défaut, on a un message simple, direct mais peu esthétique. Avec SK, nous pouvons customiser le message d'erreur. Pour cela nous allons créer un fichier spécifique : `+error.svelte`

Donc ici nous allons gérer l'interface UI que l'utilisateur va recevoir.

Pour pouvoir récupérer le message d'erreur, il faut importer un store car **l'erreur est stockée dans un store** ! Et ce store se nomme `page`. La documentation nous donne le squelette suivant :

```html
<script>
  import { page } from '$app/stores';
</script>

<h1>{$page.error.message}</h1>
```

Et nous allons pouvoir customiser la page :

```html
<script lang="ts">
    import { page } from '$app/stores';
</script>

<h1 class="is-size-1 has-text-danger">Aïe une erreur</h1>
<p>
{$page.status}: {$page.error?.message}
</p>
```

## 7-Mise à jour des données

### 1-Gestion de la mise à jour avec SQL et TS

Comment procéder si l'on veut par exemple modifier le nom d'un album ?

Dans notre fichier database.ts, nous allons rajouter une fonction exportée qui prend en argument l'ID de l'album à mettre à jour et un second argument qui sera le nouveau titre.

La requête SQL ressemblera à :

```sql
update albums set Title = XXXXXX
where AlbumId = YYYYYY
```

Nous changerons les X et les Y par les noms des paramètres.

Côté codage TS pour la base de données, nous avons vu all(), get(). Mais par le cas d'un update, nous allons utiliser une méthode particulière `run()` à laquelle on passe un objet constitué de nos deux paramètres.

Notre fonction ne retournera rien mais affichera le résultat de la requête

Le code :

```ts
export const updateAlbumTitle = (albumId: number, newAlbumTitle: string) => {
    const sql = `update albums set Title = $newAlbumTitle
where AlbumId = $albumId`;

    const rslt = db.prepare(sql).run({ newAlbumTitle, albumId })
    console.log(`Changement = ${rslt.changes}. RowId = ${rslt.lastInsertRowid}`);
}
```

### 2-Création d'un formulaire HTML

Revenons à  `./src/routes/album/[id]/+page.svelte` et ajoutons après notre tableau un formulaire :

```html
<h2 class="is-size-3 mb-4 mt-6">Update du titre de l'album</h2>

<form method="post" action="">
    <input
        class="input"
        type="text"
        name="albumTitle"
        placeholder={d.album.albumTitle}
        style="max-width: 50ch;"
    />
    <input type="hidden" name="albumId" value={d.album.albumId} />
    <button class="button is-primary" type="submit">Update</button>
</form>
```

Avec SvelteKit, nous pouvons nommer une action. L'intérêt est que nous pouvons créer et récupérer une action précise : ici juste une mise à jour et l'appliquer. Il suffit de préciser dans `action=""` avec la syntaxe `?/NomActionNommée`. **NB** : le ? indique que l'on a affaire à un paramètre de requête.

La [documentation officielle](https://kit.svelte.dev/docs/form-actions#named-actions) donne un exemple intéressant : un formulaire avec une action particulière de login et un bouton avec une action particulière mais différente d'enregistrement d'un nouvel utilisateur. Ce dernier utilise un attribut spécial `formaction` et sa syntaxe est identique à action mais l'appel de l'action nommée diffère. Et cette différence est traitée côté `+page.server.ts`

Donc notre formulaire peut s'écrire de deux façons :

- avec l'action nommée dans le formulaire

```html
<div class="m-3">
    ...

    <form method="post" action="?/updateAlbumTitle">
        ...
    </form>
</div>
```

- soit dans le bouton

```html
<div class="m-3">

    ...

    <form method="post">
        ...
        <button 
            class="button is-primary" 
            type="submit" 
            formaction="?/updateAlbumTitle">Update</button>

    </form>
</div>
```

**Nous verrons aussi qu'il n'y aura pas de redirection vers une autre page.**

### 3-Interface entre le HTML et la requête

Si la documentation officielle utilise +page.ts, nous, nous allons réutiliser le fichier `+page.server.ts` situé `./src/routes/album/[id]`.

Nous devons importer le type `Actions` depuis `./$types` :

```ts
import type { PageServerLoad, Actions } from './$types';
```

Puis créons une fonction exportée nommée `actions` suivant le modèle de la documentation :

```ts
export const actions = {
    updateAlbumTitle: async () => { }
} satisfies Actions;
```

Nous allons ici récupérer la requête et SK nous propose un mot-clé `request` récupérable en paramètre de notre action nommée :

```ts
updateAlbumTitle: async ({ request }) => { }
```

Maintenant nous allons pouvoir récupérer les données via la méthode asynchrone `formData()` qui retourne un objet ressemblant à une Map. Les valeurs sont récupérables avec la méthode `get()` à laquelle on passe le nom d'un des champs passés à la requête.

```ts
export const actions = {
    updateAlbumTitle: async ({ request }) => {
        const data = await request.formData();

        const albumIdStr = data.get("albumId")?.toString();
        const albumId = albumIdStr ? parseInt(albumIdStr) : null;

        const albumTitle = data.get("albumTitle")?.toString()

        if (!albumId && !albumTitle) {
            throw error(400, "Requête invalide")
        }

        updateAlbumTitle(albumId, albumTitle)
    }
} satisfies Actions;
```

Que se passe-t-il ? décrivons étape par étape:

- nous récupérons les données du formulaire
- nous récupérons chacun des éléments transmis par le formulaire avec conversion de type pour l'ID
- si l'ID ou le titre n'est pas valide alors il y aura erreur
- enfin si tout s'est bien déroulé alors on appelle la fonction `updateAlbumTitle`

Maintenant nous pouvons tester. Et nous verrons que la page est seulement rafraîchie avec les données mises à jour ! Tout s'est bien déroulé.

## 8-Recherche de pistes

Nous aborderons le sujet suivant : les API avec SK. Et pour cela, nous utiliserons une page spéciale : [`+server.ts`](https://kit.svelte.dev/docs/routing#server)

### 1-Recherche dans la base

Nous allons créer une nouvelle fonction dialoguant avec la base de données. Cette fonction demandera :

- un terme de recherche
- une limite

Donc nous allons tout simplement reprendre `getInitialTracks` et la modifier. Nous modifions la requête SQL pour ajouter une condition `WHERE` (pour éviter les problèmes de casse, nous passerons tout en bas de casse). Nous allons aussi passer à la méthode `all()` un objet avec nos deux arguments :

```ts
export const searchTracks = (searchTerm: string, limit: number): Track[] => {

    const sql = `select 
    t.TrackId as trackId, 
    t.Name as trackName, 
    a.AlbumId as albumId, 
    a.Title as albumTitle, 
    at.ArtistId as artistId, 
    at.Name as artistName, 
    g.Name as genre
from tracks t 
    join albums a 
        on t.AlbumId = a.AlbumId 
    join artists at 
        on a.ArtistId = at.ArtistId 
    join genres 
        g on t.GenreId = g.GenreId
where lower(t.Name) like lower('%' || $searchTerm || '%')
limit $limit
`;
    const rslt = db.prepare(sql).all({ searchTerm, limit });
    console.log(rslt);

    return rslt as Track[];
};
```

### 2-Ajout d'un formulaire de recherche

Dans `./src/routes/+page.svelte`, nous rajoutons le code HTML suivant :

```html
<script lang="ts">
    import type { PageData } from './$types';
    export let data: PageData;

    let searchTerm = '';
</script>

<div class="px-4">
    <h1 class="is-size-1 mb-5">Tracks</h1>

    <input
        type="search"
        placeholder="Search..."
        class="mb-5 input"
        style="max-width: 88ch;"
        value={searchTerm}
    />
    
    <table class="table">
        ...
```

Nous avons :

- créé un champ de recherche associée à une variable `searchTerm`
- créé cette nouvelle variable qui récupère la valeur du champ de recherche
- modifié très légèrement la balise `<h1>`

Maintenant nous devons créé une API REST pour récupérer les données de la base à l'aide uniquement de SK.

### 3-Création d'une API

Donc créons un répertoire spécifique `./src/routes/api` et à l'intérieur de celui-ci on créeun autre dossier `searchTracks` incluant un fichier spécifique qui va agir comme un serveur : `+server.ts`. C'est avec ce type de fichier que sont gérés les APIs REST.

La nomenclature veut que notre nom d'API dispose de son propre répertoire. C'est dans +server.ts que seront gérés les actions HTTP telles que GET, POST, DELETE, ... (cf. doc officielle sur [`+server.ts`](https://kit.svelte.dev/docs/routing#server))

Si on lit la documentation SK, nous tombons sur un code dont le squelette ressemble à ceci (nous n'utiliserons pas du code asynchrone) :

```ts
import type { RequestHandler } from './$types';

export const GET: RequestHandler = () => {
    return new Response();
};
```

Notre requête utilise le verbe HTTP GET puisque nous récupérons uniquement des données.

Nous passerons nos données comme paramètres d'URL. Donc nous utiliserons un objet `url` fourni par SK et nous allons récupérer les données à l'aide d'une des propriétés du type `URL` (cf. [doc officielle](https://kit.svelte.dev/docs/load#using-url-data)) : `searchParams`. Nous appellerons une méthode `get()` qui prend en paramètre une clé (le nom de notre paramètre de requête tout simplement) :

```ts
import { getInitialTracks, searchTracks } from '$lib/database';
import type { Track } from '$lib/types';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = ({ url }) => {
    const searchTerm = url.searchParams.get("searchTerm");
    let tracks: Track[] = [];

    if (searchTerm !== null) {
        tracks = searchTracks(searchTerm, 20) ?? [];

    } else {
        tracks = getInitialTracks(5);
    }
};
```

Nous avons géré les cas suivants :

- soit on dispose d'un paramètre alors on fait une recherche
- si la recherche est vide, alors on retourne un tableau vide à la variable `tracks`
- sinon on affecte le résultat à la variable `tracks`
- s'il n'y a pas de paramètre `searchTerm`, alors on appelle la fonction `getInitialTracks`

Mais que retournons-nous ? un JSON. Mais pas avec l'API JS/TS traditionnelle. Non, ce sera celle de SK.

Il faudra d'abord importer la fonction `json` :

```ts
import { json } from '@sveltejs/kit';
```

puis nous pourrons retourner directement le résultat comme suit :

```ts
export const GET: RequestHandler = ({ url }) => {
    ...

    return json(tracks);
};
```

**Nous avons notre endpoint pour notre API** avec SK, et ce, d'une façon simple et lisible. L'accès se fera avec l'URL `localhost/api/searchTerm` et lui passant un paramètre de requête.

### 4-Finalisation de l'UI

Revenons maintenant à `./src/routes/+page.svelte`.

Dans notre input, nous allons lui ajouter un événement : `on:input` et on lui associe une fonction que l'on va créer : `handleSearch`.

```html
<script lang="ts">
    import type { PageData } from './$types';
    export let data: PageData;

    let searchTerm = '';

    const handleSearch = () => {};
</script>

<div class="px-4">
    <h1 class="is-size-1 mb-5">Tracks</h1>

    <input
        type="search"
        placeholder="Search..."
        class="mb-5 input"
        style="max-width: 88ch;"
        value={searchTerm}
        on:input={handleSearch}
    />
...
```

Avec TS, nous pouvons récupérer la valeur de notre input. Pour cela notre fonction prendra en argument un type `Event`. Nous appellerons la propriété `target` sur cet objet et indiquerons à TS que cet objet est de type `HTMLInputElement`. Enfin nous allons pouvoir récupérer la valeur avec la propriété `value` :

```ts
const handleSearch = (e: Event) => {
    input = e.target as HTMLInputElement;
    console.log(input.value);
};
```

Nous allons ajouter une gestion de timeout avec TS :

```ts
let timer: NodeJS.Timeout;

const handleSearch = (e: Event) => {
clearTimeout(timer);
timer = setTimeout(() => {
    let input = e.target as HTMLInputElement;
    searchTerm = input.value;
    // la fonction de recherche que nous allons créer
    fetchTracks();
    }, 300);
};
```

Pour récupérer des données nous allons créer une fonction fetchTracks() qui va se charger de faire la requête vers notre API et retourner le résultat sur notre page :

```ts
const fetchTracks = async () => {
    const rep = await fetch(`api/searchTracks?searchTerm=${searchTerm}`);
    const data = await rep.json();
    console.log(data);
};
```

Rien de bien compliqué.

Testons le code. Et si tout se passe, côté navigateur, nous récupérons la liste des pistes à la console.

Maintenant faisons en sorte que notre tableau affiche les données. Il faut que l'on modifie notre tableau :

```html
...
        <tbody>
            {#each tracks as t}
                ...
            {/each}
        </tbody>
...
```

Donc nous allons créer une variable `tracks` qui va stocker toutes nos données :

```html
<script lang="ts">
    import type { PageData } from './$types';
    export let data: PageData;

    let tracks = data.tracks;

    ...

</script>
```

Et changeons le `console.log(data)` et `tracks = data;` :

```ts
const fetchTracks = async () => {
    const rep = await fetch(`/api/searchTracks?searchTerm=${searchTerm}`);
    const data = await rep.json();
    //console.log(data);
    tracks = data;
};
```

Et voilà notre système peut être étendu à souhait et assez simplement avec SK.
