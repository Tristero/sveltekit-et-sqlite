import { getInitialTracks, searchTracks } from '$lib/database';
import type { Track } from '$lib/types';
import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';

export const GET: RequestHandler = ({ url }) => {
    const searchTerm = url.searchParams.get("searchTerm");
    let tracks: Track[] = [];

    if (searchTerm !== null) {
        tracks = searchTracks(searchTerm, 20) ?? [];

    } else {
        tracks = getInitialTracks(5);
    }

    return json(tracks);
};