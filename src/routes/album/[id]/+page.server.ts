import { getAlbumById, getAlbumTracks, updateAlbumTitle } from '$lib/database';
import { error } from '@sveltejs/kit';
import type { PageServerLoad, Actions } from './$types';

export const load = (({ params }) => {
    const id = +params.id;

    if (Number.isNaN(id)) {
        throw error(404, "ID est non valide")
    }
    const album = getAlbumById(id)
    const tracks = getAlbumTracks(id);

    if (!album) {
        throw error(404, `Pas d'album avec l'ID ${id}`)
    }

    return {
        id,
        album,
        tracks
    };

}) satisfies PageServerLoad;

export const actions = {
    updateAlbumTitle: async ({ request }) => {
        const data = await request.formData();

        const albumIdStr = data.get("albumId")?.toString();
        const albumId = albumIdStr ? parseInt(albumIdStr) : null;

        const albumTitle = data.get("albumTitle")?.toString()

        if (!albumId || !albumTitle) {
            throw error(400, "Requête invalide")
        }

        updateAlbumTitle(albumId, albumTitle)
    }
} satisfies Actions;