import { getInitialTracks } from '$lib/database';
import type { PageServerLoad } from './$types';

export const load = (() => {
    const rlst = getInitialTracks(20);
    return {
        tracks: rlst
    };
}) satisfies PageServerLoad;