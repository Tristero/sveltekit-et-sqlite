import Database from "better-sqlite3";
import { DB_PATH } from '$env/static/private';
import type { Album, AlbumTracks, Track } from "./types";

const db = Database(DB_PATH, { verbose: console.log });

export const getInitialTracks = (limit: number): Track[] => {

    const sql = `select 
    t.TrackId as trackId, 
    t.Name as trackName, 
    a.AlbumId as albumId, 
    a.Title as albumTitle, 
    at.ArtistId as artistId, 
    at.Name as artistName, 
    g.Name as genre
from tracks t 
    join albums a 
        on t.AlbumId = a.AlbumId 
    join artists at 
        on a.ArtistId = at.ArtistId 
    join genres 
        g on t.GenreId = g.GenreId
limit ${limit}
`;
    const rslt = db.prepare(sql).all();
    console.log(rslt);

    return rslt as Track[];
};

export const getAlbumById = (id: number) => {
    const sql = `select 
   a.AlbumId as albumId,
   a.Title as albumTitle,
   at.ArtistId as artistId,
   at.Name as artistName
from albums a
join artists at on a.AlbumId = at.ArtistId
where a.AlbumId =$id`;

    const rslt = db.prepare(sql).get({ id })
    return rslt as Album;
};

export const getAlbumTracks = (id: number) => {
    const sql = `select 
 t.TrackId as trackId,
 t.Name as trackName,
 t.Milliseconds as trackMs
from tracks t
where t.AlbumId = $id
order by t.TrackId`;
    const rslt = db.prepare(sql).all({ id });
    return rslt as AlbumTracks[];
}

export const updateAlbumTitle = (albumId: number, newAlbumTitle: string) => {
    const sql = `update albums
    set Title = $newAlbumTitle
    where AlbumId = $albumId`;

    const rslt = db.prepare(sql).run({ newAlbumTitle, albumId })
    console.log(`Changement = ${rslt.changes}. RowId = ${rslt.lastInsertRowid}`);
}

export const searchTracks = (searchTerm: string, limit: number): Track[] => {

    const sql = `select 
    t.TrackId as trackId, 
    t.Name as trackName, 
    a.AlbumId as albumId, 
    a.Title as albumTitle, 
    at.ArtistId as artistId, 
    at.Name as artistName, 
    g.Name as genre
from tracks t 
    join albums a 
        on t.AlbumId = a.AlbumId 
    join artists at 
        on a.ArtistId = at.ArtistId 
    join genres 
        g on t.GenreId = g.GenreId
where lower(t.Name) like lower('%' || $searchTerm || '%')
limit $limit
`;
    const rslt = db.prepare(sql).all({ searchTerm, limit });
    console.log(rslt);

    return rslt as Track[];
};